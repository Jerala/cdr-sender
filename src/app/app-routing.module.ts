import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {SmsComponent} from './modules/sms/sms.component';
import {TrafficComponent} from './modules/traffic/traffic.component';
import {VoiceComponent} from './modules/voice/voice.component';

const routes: Routes = [
  {path: 'sms', component: SmsComponent},
  {path: 'traffic', component: TrafficComponent},
  {path: 'call', component: VoiceComponent},
  {path: '', redirectTo: '/sms', pathMatch: 'full'},
  {path: '**', redirectTo: '/sms'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
