import {Disposition, ServiceType, TrafficQuality, TrafficUnits} from '../defaults';

export interface CDRModel {
  serviceType: ServiceType;
  sourceCountryCode: number;
  sourceServiceId: number;
  startDate: string;
  disposition: Disposition;
  destinationCountryCode?: number;
  destinationServiceId?: number;
  roamingCode?: number;
  smsSize?: number;
  endDate?: string;
  trafficQuality?: TrafficQuality;
  trafficUnits?: TrafficUnits;
  trafficVolume?: number;
  webServerName?: string;
  callDuration?: number;
}
