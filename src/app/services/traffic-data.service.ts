import {Injectable} from '@angular/core';
import {CDRModel} from '../models/CDRModel';
import {Disposition, ServiceType, TrafficQuality, TrafficUnits} from '../defaults';
import {CdrGeneratorService} from './cdr-generator.service';

@Injectable({
  providedIn: 'root'
})
export class TrafficDataService {

  public cdr: CDRModel = {
    serviceType: ServiceType.TRAFFIC,
    sourceCountryCode: 7,
    sourceServiceId: 9255678912,
    startDate: this.cdrGeneratorService.toDateString(new Date()),
    endDate: this.createEndDate(),
    roamingCode: 6,
    webServerName: 'yandex',
    trafficVolume: 14,
    trafficUnits: TrafficUnits.MB,
    trafficQuality: TrafficQuality.SPEED,
    disposition: Disposition.SUCCESS
  };
  public cdrResponse: string;
  public dispositions = Disposition;
  public trafficUnits = TrafficUnits;
  public trafficQualities = TrafficQuality;
  public webServerNames = ['google', 'github', 'gitlab', 'linkedin', 'yandex'];

  constructor(private cdrGeneratorService: CdrGeneratorService) {
  }

  private createEndDate(): string {
    let endDate = new Date();
    endDate.setSeconds(endDate.getSeconds() + Math.floor(Math.random() * 400) + 1);
    return this.cdrGeneratorService.toDateString(endDate);
  }
}
