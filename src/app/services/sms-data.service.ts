import {Injectable} from '@angular/core';
import {CDRModel} from '../models/CDRModel';
import {Disposition, ServiceType} from '../defaults';
import {CdrGeneratorService} from './cdr-generator.service';

@Injectable({
  providedIn: 'root'
})
export class SmsDataService {

  public cdr: CDRModel = {
    serviceType: ServiceType.SMS,
    sourceCountryCode: 7,
    sourceServiceId: 9255678912,
    startDate: this.cdrGeneratorService.toDateString(new Date()),
    destinationCountryCode: 8,
    destinationServiceId: 2342354634,
    roamingCode: 5,
    smsSize: 14,
    disposition: Disposition.SUCCESS
  };
  public cdrResponse: string;
  public dispositions = Disposition;

  constructor(private cdrGeneratorService: CdrGeneratorService) {
  }

}
