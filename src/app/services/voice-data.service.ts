import {Injectable} from '@angular/core';
import {CdrGeneratorService} from './cdr-generator.service';
import {CDRModel} from '../models/CDRModel';
import {Disposition, ServiceType} from '../defaults';

@Injectable({
  providedIn: 'root'
})
export class VoiceDataService {

  private callDuration = 47;

  public cdr: CDRModel = {
    serviceType: ServiceType.VOICE,
    sourceCountryCode: 7,
    sourceServiceId: 9255678912,
    startDate: this.cdrGeneratorService.toDateString(new Date()),
    endDate: this.createEndDate(),
    callDuration: this.callDuration,
    destinationCountryCode: 5,
    destinationServiceId: 3568376947,
    roamingCode: 6,
    disposition: Disposition.SUCCESS
  };
  public cdrResponse: string;
  public dispositions = Disposition;

  constructor(private cdrGeneratorService: CdrGeneratorService) {
  }

  private createEndDate(): string {
    let endDate = new Date();
    endDate.setSeconds(endDate.getSeconds() + this.callDuration);
    return this.cdrGeneratorService.toDateString(endDate);
  }
}
