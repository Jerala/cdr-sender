import {Injectable} from '@angular/core';
import {CDRModel} from '../models/CDRModel';
import {Disposition, serviceIds, ServiceType, TrafficQuality, TrafficUnits} from '../defaults';

@Injectable({
  providedIn: 'root'
})
export class CdrGeneratorService {

  constructor() {
  }

  public getRandomSMSCDR(): CDRModel {
    const dispositions = Object.values(Disposition);
    let cdr: CDRModel = {
      serviceType: ServiceType.SMS,
      sourceCountryCode: 7,
      sourceServiceId: serviceIds[Math.floor(Math.random() * (serviceIds.length))],
      startDate: this.toDateString(this.generateRandomDate()),
      destinationCountryCode: Math.floor(Math.random() * 9) + 1,
      destinationServiceId: 1000000000 + Math.floor(Math.random() * 999999999),
      roamingCode: Math.floor(Math.random() * 9) + 1,
      smsSize: Math.floor(Math.random() * 1000) + 1,
      disposition: dispositions[Math.floor(Math.random() * (dispositions.length))]
    };
    return cdr;
  }

  public getRandomVoiceCDR(): CDRModel {
    let startDate = this.generateRandomDate();
    let callDuration = Math.floor(Math.random() * 140);
    let endDate = new Date(startDate.toString());
    endDate.setSeconds(startDate.getSeconds() + callDuration);
    const dispositions = Object.values(Disposition);
    let cdr: CDRModel = {
      serviceType: ServiceType.VOICE,
      sourceCountryCode: 7,
      sourceServiceId: serviceIds[Math.floor(Math.random() * (serviceIds.length))],
      startDate: this.toDateString(startDate),
      endDate: this.toDateString(endDate),
      callDuration: callDuration,
      destinationCountryCode: Math.floor(Math.random() * 9) + 1,
      destinationServiceId: 1000000000 + Math.floor(Math.random() * 999999999),
      roamingCode: Math.floor(Math.random() * 9) + 1,
      disposition: dispositions[Math.floor(Math.random() * (dispositions.length))]
    };
    return cdr;
  }

  public getRandomTrafficCDR(): CDRModel {
    let startDate = this.generateRandomDate();
    let duration = Math.floor(Math.random() * 400);
    let endDate = new Date(startDate.toString());
    endDate.setSeconds(startDate.getSeconds() + duration);
    const dispositions = Object.values(Disposition);
    const trafficQualities = Object.values(TrafficQuality);
    const trafficUnits = Object.values(TrafficUnits);
    const sites = ['google', 'github', 'gitlab', 'linkedin', 'yandex'];
    let cdr: CDRModel = {
      serviceType: ServiceType.TRAFFIC,
      sourceCountryCode: 7,
      sourceServiceId: serviceIds[Math.floor(Math.random() * (serviceIds.length))],
      startDate: this.toDateString(startDate),
      endDate: this.toDateString(endDate),
      roamingCode: Math.floor(Math.random() * 9) + 1,
      webServerName: sites[Math.floor(Math.random() * (sites.length))],
      trafficVolume: Math.floor(Math.random() * 100) + 1,
      trafficUnits: trafficUnits[Math.floor(Math.random() * (trafficUnits.length))],
      trafficQuality: trafficQualities[Math.floor(Math.random() * (trafficQualities.length))],
      disposition: dispositions[Math.floor(Math.random() * (dispositions.length))]
    };
    return cdr;
  }

  public generateRandomDate(): Date {
    const firstDate = new Date(2018, 1, 1, 1, 0, 0);
    const secondDate = new Date();
    return new Date(firstDate.getTime()
      + Math.random() * (secondDate.getTime() - firstDate.getTime()));
  }

  public toDateString(date: Date): string {
    return (date.getFullYear().toString() + '-'
      + ('0' + (date.getMonth() + 1)).slice(-2) + '-'
      + ('0' + (date.getDate())).slice(-2))
      + 'T' + date.toTimeString().slice(0, 8);
  }
}
