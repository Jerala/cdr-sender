import {Injectable} from '@angular/core';
import {BACKEND_URL, CONTROLLER_URL} from '../defaults';
import {CDRModel} from '../models/CDRModel';
import {Observable} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SenderService {

  constructor(private http: HttpClient) {
  }

  public sendCDR(cdr: CDRModel): Observable<any> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
    });
    return this.http.post(BACKEND_URL + CONTROLLER_URL, JSON.stringify(cdr), {headers: headers});
  }

}
