import {Component, OnInit} from '@angular/core';
import {SenderService} from '../../services/sender.service';
import {CdrGeneratorService} from '../../services/cdr-generator.service';
import {SmsDataService} from '../../services/sms-data.service';

@Component({
  selector: 'app-sms',
  templateUrl: './sms.component.html',
  styleUrls: ['./sms.component.css']
})
export class SmsComponent implements OnInit {

  constructor(private senderService: SenderService,
              private generatorService: CdrGeneratorService,
              /** @internal */
              public _smsDataService: SmsDataService) {
  }

  ngOnInit() {
  }

  /** @internal */
  public _sendSMS(): void {
    this.senderService.sendCDR(this._smsDataService.cdr).subscribe((resp) => {
      this._smsDataService.cdrResponse = resp;
    });
  }

  /** @internal */
  public _generateRandomSMSCDR(): void {
    this._smsDataService.cdr = this.generatorService.getRandomSMSCDR();
  }

}
