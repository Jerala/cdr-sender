import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SmsComponent} from './sms.component';
import {FormsModule} from '@angular/forms';
import {ClarityModule} from '@clr/angular';
import {PipeModule} from '../../pipes/pipe.module';

@NgModule({
  declarations: [SmsComponent],
  imports: [
    CommonModule,
    FormsModule,
    ClarityModule,
    PipeModule
  ],
  exports: [SmsComponent]
})
export class SmsModule {
}
