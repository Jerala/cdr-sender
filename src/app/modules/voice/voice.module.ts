import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {VoiceComponent} from './voice.component';
import {FormsModule} from '@angular/forms';
import {ClarityModule} from '@clr/angular';
import {PipeModule} from '../../pipes/pipe.module';

@NgModule({
  declarations: [VoiceComponent],
  imports: [
    CommonModule,
    FormsModule,
    ClarityModule,
    PipeModule
  ],
  exports: [VoiceComponent]
})
export class VoiceModule {
}
