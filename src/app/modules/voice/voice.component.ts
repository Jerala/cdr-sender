import {Component, OnInit} from '@angular/core';
import {SenderService} from '../../services/sender.service';
import {CdrGeneratorService} from '../../services/cdr-generator.service';
import {VoiceDataService} from '../../services/voice-data.service';

@Component({
  selector: 'app-call',
  templateUrl: './voice.component.html',
  styleUrls: ['./voice.component.css']
})
export class VoiceComponent implements OnInit {

  constructor(private senderService: SenderService,
              private generatorService: CdrGeneratorService,
              /** @internal */
              public _voiceDataService: VoiceDataService) {
  }

  ngOnInit() {
  }

  /** @internal */
  public _sendVoice(): void {
    this.senderService.sendCDR(this._voiceDataService.cdr).subscribe((resp) => {
      this._voiceDataService.cdrResponse = resp;
    });
  }

  /** @internal */
  public _generateRandomVoiceCDR(): void {
    this._voiceDataService.cdr = this.generatorService.getRandomVoiceCDR();
  }


}
