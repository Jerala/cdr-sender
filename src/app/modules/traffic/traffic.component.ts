import {Component, OnInit} from '@angular/core';
import {SenderService} from '../../services/sender.service';
import {CdrGeneratorService} from '../../services/cdr-generator.service';
import {TrafficDataService} from '../../services/traffic-data.service';

@Component({
  selector: 'app-traffic',
  templateUrl: './traffic.component.html',
  styleUrls: ['./traffic.component.css']
})
export class TrafficComponent implements OnInit {

  constructor(private senderService: SenderService,
              private generatorService: CdrGeneratorService,
              /** @internal */
              public _trafficDataService: TrafficDataService) {
  }

  ngOnInit() {
  }

  /** @internal */
  public _sendTraffic(): void {
    this.senderService.sendCDR(this._trafficDataService.cdr).subscribe((resp) => {
      this._trafficDataService.cdrResponse = resp;
    });
  }

  /** @internal */
  public _generateRandomTrafficCDR(): void {
    this._trafficDataService.cdr = this.generatorService.getRandomTrafficCDR();
  }

}
