import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TrafficComponent} from './traffic.component';
import {FormsModule} from '@angular/forms';
import {ClarityModule} from '@clr/angular';
import {PipeModule} from '../../pipes/pipe.module';

@NgModule({
  declarations: [TrafficComponent],
  imports: [
    CommonModule,
    FormsModule,
    ClarityModule,
    PipeModule
  ],
  exports: [TrafficComponent]
})
export class TrafficModule {
}
