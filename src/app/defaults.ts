export const BACKEND_URL = 'http://localhost:8080';
export const CONTROLLER_URL = '/add/cdr';

export enum Disposition {
  NONE = 'NONE', SUCCESS = 'SUCCESS', UNREACHABLE = 'UNREACHABLE', ABORTED = 'ABORTED'
}

export enum Status {
  NONE = 'NONE', RECEIVED = 'RECEIVED', DUPLICATE = 'DUPLICATE',
  ERROR = 'ERROR', RATED = 'RATED', BILLED = 'BILLED'
}

export enum TrafficUnits {
  B = 'B', KB = 'KB', MB = 'MB', GB = 'GB'
}

export enum TrafficQuality {
  PRIORITY = 'PRIORITY', SPEED = 'SPEED', LEASEDLINE = 'LEASEDLINE'
}

export enum ServiceType {
  VOICE = 'VOICE', SMS = 'SMS', TRAFFIC = 'TRAFFIC'
}

export const serviceIds = [ 9255678912, 9259881285 ];
