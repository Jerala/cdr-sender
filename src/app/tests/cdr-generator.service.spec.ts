import { TestBed } from '@angular/core/testing';

import { CdrGeneratorService } from '../services/cdr-generator.service';

describe('CdrGeneratorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CdrGeneratorService = TestBed.get(CdrGeneratorService);
    expect(service).toBeTruthy();
  });
});
