import { TestBed } from '@angular/core/testing';

import { VoiceDataService } from '../services/voice-data.service';

describe('VoiceDataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: VoiceDataService = TestBed.get(VoiceDataService);
    expect(service).toBeTruthy();
  });
});
