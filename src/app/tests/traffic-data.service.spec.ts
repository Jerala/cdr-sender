import { TestBed } from '@angular/core/testing';

import { TrafficDataService } from '../services/traffic-data.service';

describe('TrafficDataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TrafficDataService = TestBed.get(TrafficDataService);
    expect(service).toBeTruthy();
  });
});
